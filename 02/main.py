PATH = "02/input.txt"

def step_one():
    valid = 0
    with open(PATH, "r") as f:
        lines = f.read().splitlines()
        for l in lines:
            values = l.split(" ")
            min_val, max_val = values[0].split("-")
            letter = values[1].replace(":","")
            password = values[2]

            count = 0
            for p in password:
                if p == letter:
                    count += 1 
            if count >= int(min_val) and count <= int(max_val):
                valid += 1 
    print(valid)

def step_two():
    valid = 0
    with open(PATH, "r") as f:
        lines = f.read().splitlines()
        for l in lines:
            values = l.split(" ")
            pos_one, pos_two = values[0].split("-")
            letter = values[1].replace(":","")
            password = values[2]

            first = password[int(pos_one) - 1]
            second = password[int(pos_two) - 1]
            if first == letter and second != letter or first != letter and second == letter:
                valid += 1
    print(valid)


step_one()
step_two()
