PATH = "01/input.txt"

numbers = []
with open(PATH, "r") as f:
	numbers = f.read().splitlines() 

numbers = [int(i) for i in numbers]
for idx, i in enumerate(numbers):
    for jdx, j in enumerate(numbers):
        for kdx, k in enumerate(numbers):
            if i != j != k and i + j + k == 2020:
                print(i*j*k)
